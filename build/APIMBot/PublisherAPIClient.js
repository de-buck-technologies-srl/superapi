"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var https_1 = __importDefault(require("https"));
var APIMAuthClient_1 = __importDefault(require("./APIMAuthClient"));
var fs = __importStar(require("fs"));
var form_data_1 = __importDefault(require("form-data"));
var PublisherAPIClient = /** @class */ (function (_super) {
    __extends(PublisherAPIClient, _super);
    function PublisherAPIClient() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PublisherAPIClient.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.getToken()];
                    case 1:
                        _a.token = _b.sent();
                        this.api = axios_1.default.create({
                            baseURL: "".concat(this.conf.serverletURL, "/api/am/publisher/v1/apis"),
                            httpsAgent: new https_1.default.Agent({ rejectUnauthorized: false }),
                            transformRequest: [
                                function (data, headers) {
                                    if (headers) {
                                        headers["Content-type"] = "application/json";
                                        headers["Authorization"] = "Bearer ".concat(_this.token);
                                    }
                                    return JSON.stringify(data);
                                },
                            ],
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    PublisherAPIClient.prototype.search = function (search) {
        return __awaiter(this, void 0, void 0, function () {
            var data, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.api.get("/?query=" + search, {
                                params: {
                                    limit: 1499,
                                    expand: true,
                                    // query: search,
                                },
                            })];
                    case 1:
                        data = (_a.sent()).data;
                        return [2 /*return*/, data.list];
                    case 2:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [2 /*return*/, ([])];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PublisherAPIClient.prototype.foundBugIndex = function (search, start, stop, offset, timeIntervalInMs) {
        if (offset === void 0) { offset = 1; }
        if (timeIntervalInMs === void 0) { timeIntervalInMs = 50; }
        return __awaiter(this, void 0, void 0, function () {
            var err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(start < stop)) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        console.log("testing: ".concat(search, " [").concat(start, ", ").concat(start + offset, "]"));
                        return [4 /*yield*/, this.api.get("/", {
                                params: {
                                    query: search,
                                    expand: true,
                                    limit: offset,
                                    offset: start
                                },
                            })];
                    case 2:
                        _a.sent();
                        start += offset;
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.log({ msg: "error while retreiving apis", start: start, end: start + offset });
                        console.log(err_1);
                        process.exit();
                        return [3 /*break*/, 4];
                    case 4: return [3 /*break*/, 0];
                    case 5:
                        console.log("waiting for errors");
                        return [4 /*yield*/, new Promise(function (r) { return setTimeout(r, 80000000); })
                            // return data.list;
                        ];
                    case 6:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PublisherAPIClient.prototype.getApiByResultIndex = function (index, full, search) {
        if (full === void 0) { full = true; }
        if (search === void 0) { search = ""; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (res, rej) {
                        var req = _this.api.get("/", {
                            params: {
                                query: search,
                                expand: full,
                                limit: 1,
                                offset: index
                            }
                        });
                        req.then(function (_a) {
                            var data = _a.data;
                            return res(data.list);
                        })
                            .catch(function (err) { return rej(err); });
                    })];
            });
        });
    };
    PublisherAPIClient.prototype.searchWithDetails = function (search) {
        return __awaiter(this, void 0, void 0, function () {
            var results, e_2, detailedResults, _i, results_1, result, details;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        results = [];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.search(search)];
                    case 2:
                        results = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        console.log(e_2);
                        return [3 /*break*/, 4];
                    case 4:
                        detailedResults = [];
                        _i = 0, results_1 = results;
                        _a.label = 5;
                    case 5:
                        if (!(_i < results_1.length)) return [3 /*break*/, 8];
                        result = results_1[_i];
                        return [4 /*yield*/, this.getDetails(result.id)];
                    case 6:
                        details = _a.sent();
                        detailedResults.push(details);
                        _a.label = 7;
                    case 7:
                        _i++;
                        return [3 /*break*/, 5];
                    case 8: return [2 /*return*/, detailedResults];
                }
            });
        });
    };
    PublisherAPIClient.prototype.getDetails = function (apiId) {
        return __awaiter(this, void 0, void 0, function () {
            var data, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.api.get("/".concat(apiId))];
                    case 1:
                        data = (_a.sent()).data;
                        return [2 /*return*/, data];
                    case 2:
                        e_3 = _a.sent();
                        return [2 /*return*/, __assign({}, e_3)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PublisherAPIClient.prototype.create = function (apiInfo, shouldPublish) {
        return __awaiter(this, void 0, void 0, function () {
            var alreadyExistingApi, lifeCycleStatus, provider, api, provider, api, newApiInfo, myApi, myError, err_2, myError, myError;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        return [4 /*yield*/, this.exist(apiInfo)];
                    case 1:
                        alreadyExistingApi = _a.sent();
                        if (!!alreadyExistingApi) return [3 /*break*/, 3];
                        if (shouldPublish) {
                            lifeCycleStatus = apiInfo.lifeCycleStatus, provider = apiInfo.provider, api = __rest(apiInfo, ["lifeCycleStatus", "provider"]);
                        }
                        else {
                            provider = apiInfo.provider, api = __rest(apiInfo, ["provider"]);
                        }
                        return [4 /*yield*/, this.api.post("/", __assign({}, api))];
                    case 2:
                        newApiInfo = (_a.sent()).data;
                        myApi = newApiInfo || alreadyExistingApi;
                        return [2 /*return*/, shouldPublish ? this.publish(myApi) : myApi];
                    case 3:
                        if (shouldPublish && alreadyExistingApi.lifeCycleStatus == 'CREATED') {
                            this.publish(alreadyExistingApi);
                        }
                        else {
                            myError = { "error": "API exists already " + apiInfo.context + "/" + apiInfo.version + ".", "id": apiInfo.id };
                            process.stderr.write(JSON.stringify(myError) + "\n");
                            return [2 /*return*/, myError];
                        }
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        err_2 = _a.sent();
                        // * il n'y a pas de raison mais bon
                        // * je ne peux pas mettre l'error handler au niveau d'axios
                        // * parce qu'il prends les 404 pour des erreurs
                        if (err_2.response) {
                            myError = err_2.response.data;
                            myError.id = apiInfo.id;
                            // console.log("catching it here " + err.response.data.description)
                            process.stderr.write(JSON.stringify(myError) + '\n');
                            // console.log(JSON.stringify(myError) + '\n')
                            return [2 /*return*/, myError];
                        }
                        else {
                            myError = err_2;
                            myError.id = apiInfo.id;
                            // console.log("Or better it here " + err.description)
                            process.stderr.write(JSON.stringify(myError) + '\n');
                            // console.log(JSON.stringify(myError) + '\n');
                            return [2 /*return*/, myError];
                        }
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    PublisherAPIClient.prototype.createNewVersion = function (apiInfo, shouldPublish) {
        return __awaiter(this, void 0, void 0, function () {
            var alreadyExistingApi, newApiInfo, newlyPublished, _a, e_4, err_3, myError, myError;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 10, , 11]);
                        return [4 /*yield*/, this.getAnyVersion(apiInfo)];
                    case 1:
                        alreadyExistingApi = _b.sent();
                        return [4 /*yield*/, this.api.post("/copy-api", null, {
                                params: {
                                    apiId: alreadyExistingApi.id,
                                    newVersion: apiInfo.version,
                                    defaultVersion: false
                                    // policies: ["Unlimited"], //We need this to publish
                                }
                            })
                            // console.log("newly copied api id " + newApiInfo.id);
                        ];
                    case 2:
                        newApiInfo = (_b.sent()).data;
                        // console.log("newly copied api id " + newApiInfo.id);
                        delete apiInfo.id;
                        delete apiInfo.owner;
                        return [4 /*yield*/, this.update(newApiInfo.id, apiInfo)];
                    case 3:
                        _b.sent();
                        _b.label = 4;
                    case 4:
                        _b.trys.push([4, 8, , 9]);
                        if (!shouldPublish) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.publish(newApiInfo)];
                    case 5:
                        _a = _b.sent();
                        return [3 /*break*/, 7];
                    case 6:
                        _a = apiInfo;
                        _b.label = 7;
                    case 7:
                        newlyPublished = _a;
                        return [2 /*return*/, newlyPublished];
                    case 8:
                        e_4 = _b.sent();
                        console.log("error publishing new version " + e_4);
                        return [2 /*return*/, apiInfo];
                    case 9: return [3 /*break*/, 11];
                    case 10:
                        err_3 = _b.sent();
                        // * il n'y a pas de raison mais bon
                        // * je ne peux pas mettre l'error handler au niveau d'axios
                        // * parce qu'il prends les 404 pour des erreurs
                        if (err_3.response) {
                            myError = err_3.response.data;
                            // console.log("catching it here " + err.response.data.description)
                            process.stderr.write(JSON.stringify(myError) + '\n');
                            // console.log(JSON.stringify(myError) + '\n')
                            return [2 /*return*/, myError];
                        }
                        else {
                            myError = err_3;
                            // console.log("Or better it here " + err.description)
                            process.stderr.write(JSON.stringify(myError) + '\n');
                            // console.log(JSON.stringify(myError) + '\n');
                            return [2 /*return*/, myError];
                        }
                        return [3 /*break*/, 11];
                    case 11: return [2 /*return*/];
                }
            });
        });
    };
    PublisherAPIClient.prototype.getMediatorsFromAPI = function (apiId) {
        return __awaiter(this, void 0, void 0, function () {
            var e_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.api.get("/".concat(apiId, "/mediation-policies"))];
                    case 1: return [2 /*return*/, (_a.sent()).data.list];
                    case 2:
                        e_5 = _a.sent();
                        console.log(e_5);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PublisherAPIClient.prototype.addMediatorByFileName = function (api, type, filename) {
        return __awaiter(this, void 0, void 0, function () {
            var inlineFile, formData, result, mediationPolicy, plusShared, republish, e_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        inlineFile = fs.createReadStream(filename);
                        formData = new form_data_1.default();
                        formData.append('inlineContent', inlineFile);
                        formData.append('type', type);
                        return [4 /*yield*/, (0, axios_1.default)({
                                httpsAgent: new https_1.default.Agent({ rejectUnauthorized: false }),
                                method: 'post',
                                url: "".concat(this.conf.serverletURL, "/api/am/publisher/v1/apis/").concat(api.id, "/mediation-policies"),
                                data: formData,
                                headers: __assign({ "Authorization": "Bearer ".concat(this.token) }, formData.getHeaders())
                            })];
                    case 1:
                        result = _a.sent();
                        if (!(result.status === 201)) return [3 /*break*/, 3];
                        mediationPolicy = {
                            mediationPolicies: []
                        };
                        plusShared = result.data;
                        plusShared.shared = false;
                        if (Array.isArray(api.mediationPolicies)) {
                            api.mediationPolicies.push(plusShared);
                        }
                        else {
                            mediationPolicy.mediationPolicies.push(plusShared);
                            api.mediationPolicies = mediationPolicy.mediationPolicies;
                        }
                        return [4 /*yield*/, this.update(api.id, api)];
                    case 2:
                        republish = _a.sent();
                        return [2 /*return*/, republish];
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        e_6 = _a.sent();
                        console.log(e_6);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    PublisherAPIClient.prototype.removeAllMediators = function (api) {
        return __awaiter(this, void 0, void 0, function () {
            var list;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.api.get("/".concat(api.id, "/mediation-policies"))];
                    case 1:
                        list = (_a.sent()).data.list;
                        list.map(function (mediator) { return __awaiter(_this, void 0, void 0, function () {
                            var result, e_7;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        _a.trys.push([0, 2, , 3]);
                                        return [4 /*yield*/, this.removeMediatorByID(api, mediator.id)];
                                    case 1:
                                        result = _a.sent();
                                        return [3 /*break*/, 3];
                                    case 2:
                                        e_7 = _a.sent();
                                        console.log(e_7);
                                        return [3 /*break*/, 3];
                                    case 3: return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    PublisherAPIClient.prototype.removeMediatorByID = function (api, mediatorId) {
        return __awaiter(this, void 0, void 0, function () {
            var republish, response, e_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        this.removeObjById(api.mediationPolicies, mediatorId);
                        if (api.mediationPolicies && api.mediationPolicies.length < 1) {
                            delete api.mediationPolicies;
                        }
                        return [4 /*yield*/, this.update(api.id, api)];
                    case 1:
                        republish = _a.sent();
                        return [4 /*yield*/, this.api.delete("/".concat(api.id, "/mediation-policies/").concat(mediatorId))];
                    case 2:
                        response = _a.sent();
                        console.log("Mediator '".concat(mediatorId, "' deleted from API with id ").concat(api.id, "."));
                        return [2 /*return*/, response];
                    case 3:
                        e_8 = _a.sent();
                        console.log("Error deleting mediator '".concat(mediatorId, "' from API with id ").concat(api.id), +e_8);
                        return [2 /*return*/, e_8];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    PublisherAPIClient.prototype.removeObjById = function (arr, id) {
        try {
            var index = arr.findIndex(function (obj) { return obj.id === id; });
            if (index > -1) {
                arr.splice(index, 1);
            }
        }
        catch (e) {
            console.log("Couldn't find mediator with id " + id + " in the API.");
        }
    };
    PublisherAPIClient.prototype.delete = function (apiId) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.api.delete("/".concat(apiId))];
                    case 1:
                        data = (_a.sent()).data;
                        return [2 /*return*/, data];
                }
            });
        });
    };
    PublisherAPIClient.prototype.deleteByName = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var apis_2, _i, apis_1, api, err_4, apis, selected;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(name == "*" || name == "build")) return [3 /*break*/, 8];
                        return [4 /*yield*/, this.search()];
                    case 1:
                        apis_2 = _a.sent();
                        _i = 0, apis_1 = apis_2;
                        _a.label = 2;
                    case 2:
                        if (!(_i < apis_1.length)) return [3 /*break*/, 7];
                        api = apis_1[_i];
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.delete(api.id)];
                    case 4:
                        _a.sent();
                        return [3 /*break*/, 6];
                    case 5:
                        err_4 = _a.sent();
                        console.log(err_4.response.data);
                        return [3 /*break*/, 6];
                    case 6:
                        _i++;
                        return [3 /*break*/, 2];
                    case 7: return [2 /*return*/, { msg: "done" }];
                    case 8: return [4 /*yield*/, this.search("name:".concat(name))];
                    case 9:
                        apis = _a.sent();
                        selected = apis.find(function (api) { return api.name == name; });
                        if (!selected) return [3 /*break*/, 11];
                        return [4 /*yield*/, this.delete(selected.id)];
                    case 10:
                        _a.sent();
                        return [2 /*return*/, { msg: "done" }];
                    case 11: return [2 /*return*/, { msg: "not matching api found" }];
                }
            });
        });
    };
    /**
     * Publish the API
     * @param apiInfo
     * @returns the expanded API NDJSON
     */
    PublisherAPIClient.prototype.publish = function (apiInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var data, err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        if (!!apiInfo.policies) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.update(apiInfo.id, { policies: ["Unlimited"] })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        // * on passe le lifecycle à publish
                        console.log("publishing new api version");
                        return [4 /*yield*/, this.api.post("/change-lifecycle", null, {
                                params: {
                                    apiId: apiInfo.id,
                                    action: "Publish",
                                },
                            })];
                    case 3:
                        data = (_a.sent()).data;
                        return [3 /*break*/, 5];
                    case 4:
                        err_5 = _a.sent();
                        console.log(err_5.response.data);
                        return [3 /*break*/, 5];
                    case 5: return [4 /*yield*/, this.getDetails(apiInfo.id)];
                    case 6: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * Will deprecate the API in stdin
     * @param apiInfo NDJSON API
     * @returns the new API as NDJSON
     */
    PublisherAPIClient.prototype.deprecate = function (apiInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var apiId, paramsDeprecate, url, _a, paramsRetire, err_6;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        apiId = apiInfo.id || apiInfo;
                        paramsDeprecate = new URLSearchParams({
                            apiId: apiId,
                            action: "Deprecate"
                        }).toString();
                        url = "/change-lifecycle?" + paramsDeprecate;
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.api.post(url, {}, {})];
                    case 2:
                        _b.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        _a = _b.sent();
                        process.stderr.write(JSON.stringify({ error: "while changing lifecycle to Deprecate" }));
                        return [3 /*break*/, 4];
                    case 4:
                        paramsRetire = new URLSearchParams({
                            apiId: apiId,
                            action: "Retire"
                        }).toString();
                        url = "/change-lifecycle?" + paramsRetire;
                        _b.label = 5;
                    case 5:
                        _b.trys.push([5, 7, , 8]);
                        return [4 /*yield*/, this.api.post(url, {}, {})];
                    case 6:
                        _b.sent();
                        return [3 /*break*/, 8];
                    case 7:
                        err_6 = _b.sent();
                        process.stderr.write(JSON.stringify({ error: "while changing lifecycle to Retire" }));
                        return [3 /*break*/, 8];
                    case 8: return [4 /*yield*/, this.getDetails(apiId)];
                    case 9: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    /**
     * tell us if the api already exist
     * @param apiInfo an object that give the name context and version of the api
     * @returns false or the found API as NDJSON
     */
    PublisherAPIClient.prototype.exist = function (apiInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.search("context:".concat(apiInfo.context, "/").concat(apiInfo.version)
                        // `name:${apiInfo.name}` // context:${apiInfo.context} version:${apiInfo.context} // bizarre ça marche pas avec ça en plus
                        )];
                    case 1:
                        result = _a.sent();
                        // console.log("oui1");
                        return [2 /*return*/, result[0] || false];
                }
            });
        });
    };
    /**
     * Tries to get an existing version
     * @param apiInfo an object that give the name context and version of the api
     * @returns false or the found API as NDJSON
     */
    PublisherAPIClient.prototype.getAnyVersion = function (apiInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.search("context:".concat(apiInfo.context)
                            // `name:${apiInfo.name}` // context:${apiInfo.context} version:${apiInfo.context} // bizarre ça marche pas avec ça en plus
                            )];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.log("error searching " + error_1);
                        return [3 /*break*/, 3];
                    case 3: 
                    // console.log("oui1");
                    // console.log("One such previous version id " + result[0].id)
                    return [2 /*return*/, result[0] || false];
                }
            });
        });
    };
    PublisherAPIClient.prototype.update = function (apiId, update) {
        return __awaiter(this, void 0, void 0, function () {
            var error, updatedAPI, retries, success, response, e_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        error = "";
                        updatedAPI = undefined;
                        retries = 5;
                        success = false;
                        _a.label = 1;
                    case 1:
                        if (!(retries > 0 && !success)) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.api.put("/".concat(apiId), update)];
                    case 3:
                        response = _a.sent();
                        if (response.status >= 200 && response.status < 400) {
                            success = true;
                            updatedAPI = response;
                            return [3 /*break*/, 6];
                        }
                        return [3 /*break*/, 5];
                    case 4:
                        e_9 = _a.sent();
                        console.log(e_9);
                        error = e_9;
                        return [3 /*break*/, 5];
                    case 5:
                        retries--;
                        return [3 /*break*/, 1];
                    case 6: return [2 /*return*/, updatedAPI.data || error];
                }
            });
        });
    };
    return PublisherAPIClient;
}(APIMAuthClient_1.default));
var Mediator = /** @class */ (function () {
    function Mediator() {
    }
    return Mediator;
}());
var MediatorSearchResult = /** @class */ (function () {
    function MediatorSearchResult() {
    }
    return MediatorSearchResult;
}());
exports.default = PublisherAPIClient;
