"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.readStdIn = exports.getConfig = void 0;
var getConfig_1 = __importDefault(require("./getConfig"));
exports.getConfig = getConfig_1.default;
var readStdin_1 = __importDefault(require("./readStdin"));
exports.readStdIn = readStdin_1.default;
