"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This function reads from the standard input and process any input
 */
var readStdIn = function () {
    return new Promise(function (res) {
        process.stdin.resume();
        process.stdin.setEncoding("utf8");
        var output = "";
        process.stdin.on("data", function (chunk) {
            output += chunk;
        });
        process.stdin.on("end", function () {
            res(output);
        });
    });
};
exports.default = readStdIn;
