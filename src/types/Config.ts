export interface AppConfig {
  name: string;
  serverletURL: string;
  gatewayURL: string;
  clientId: string;
  clientSecret: string;
}

export interface Config {
  environments: AppConfig[];
}
