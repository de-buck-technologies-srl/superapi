import axios, { AxiosInstance } from "axios";
import https from "https";
import APIMAuthClient from "./APIMAuthClient";

class AppsAPIClient extends APIMAuthClient {
  private token?: string;
  private api: AxiosInstance;
  async login() {
    this.token = await this.getToken();

    this.api = axios.create({
      baseURL: `${this.conf.serverletURL}/api/am/store/v1/applications`,
      httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      transformRequest: [
        (data, headers) => {
          if (headers) {
            headers["Content-type"] = "application/json";
            headers["Authorization"] = `Bearer ${this.token}`;
          }
          return JSON.stringify(data);
        },
      ],
    });
  }

  async search(search: string) {
    const { data } = await this.api.get("/", {
      params: {
        query: search,
        limit: 1000,
      },
    });
    return data.list;
  }

  async searchByName(name: string) {
    return await this.search(`name:${name}`);
  }

  async create({ name, throttlingPolicy = "Unlimited", ...others }: any) {
    const body = {
      name,
      throttlingPolicy,
      ...others,
    };

    const { data } = await this.api.post("/", body);
    return data;
  }

  // * pas trop sur de si cette methode est utile
  async getDetails(appId: string) {
    const { data } = await this.api.get(`/${appId}`);

    return data;
  }

  async delete(appId: string) {
    const { data } = await this.api.delete(`/${appId}`);

    return data;
  }

  async deleteByName(name: string) {
    if (name == "*" || name == "build") {
      const apps: any[] = await this.search("name:*");

      for (let app of apps) {
        await this.delete(app);
      }

      return { msg: "done", appsDeleted: apps.map((app) => app.name) };
    }

    const apps = await this.search(name);
    if (apps.length > 0) {
      const app = apps[0];
      await this.delete(app.applicationId);
      return { msg: `${app.name} deleted` };
    }
    return { msg: "not matching app found" };
  }

  async getCredentials(appId: string, keyType: "PRODUCTION" | "SANDBOX") {
    const { data } = await this.api.get(`/${appId}/api-keys/${keyType}`);

    return data;
  }


  async generateCredentials(applicationId: string, keyType: "PRODUCTION" | "SANDBOX") {

    const data = await this.api.post(`/${applicationId}/generate-keys`, {
      keyType,              // <- obligatoire
      /*"keyManager": "Resident Key Manager",*/
      "grantTypesToBeSupported": [          // <- obligatoire
        "password",
        "client_credentials"
      ],
      /*"callbackUrl": "http://sample.com/callback/url",
      "scopes": [
        "am_application_scope",
        "default"
      ],
      "validityTime": 3600,
      "additionalProperties": {}*/
    });
  }

  async subscribe(app: any, apis: any[]) {
    const url = `${this.conf.serverletURL}/api/am/store/v1/subscriptions/multiple`;

    const { applicationId } = app;
    const body = apis.map(({ id: apiId }) => ({
      apiId,
      applicationId,
      throttlingPolicy: "Unlimited",
      requestedThrottlingPolicy: "Unlimited",
      status: "UNBLOCKED",
    }));
    try {
      const { data } = await this.api.post(url, body);
      return data;
    } catch (err) {
      console.log(err.response.data);
      // process.exit();
    }
  }
}

export default AppsAPIClient;
