import axios, { AxiosInstance } from "axios";
import https from "https";
import APIMAuthClient from "./APIMAuthClient";
import * as fs from 'fs';
import FormData from "form-data";

class PublisherAPIClient extends APIMAuthClient {
  private token?: string;
  private api: AxiosInstance;

  async login() {
    this.token = await this.getToken();

    this.api = axios.create({
      baseURL: `${this.conf.serverletURL}/api/am/publisher/v1/apis`,
      httpsAgent: new https.Agent({ rejectUnauthorized: false }),
      transformRequest: [
        (data, headers) => {
          if (headers) {
            headers["Content-type"] = "application/json";
            headers["Authorization"] = `Bearer ${this.token}`;
          }
          return JSON.stringify(data);
        },
      ],
    });
  }

  async search(search?: string) {
    // console.log("searching for " + search)
    try {
      const { data } = await this.api.get("/?query=" + search
      , {
        params: {
          limit: 1499,
          expand: true,
          // query: search,
        },
      }
      );
      return data.list;
    }catch(e) {
      console.log(e)
      return([])
    }
  }


  async foundBugIndex(search: string, start: number, stop: number, offset: number = 1, timeIntervalInMs: number = 50) {
    while (start < stop) {
      try {
        console.log(`testing: ${search} [${start}, ${start + offset}]`)
        await this.api.get("/", {
          params: {
            query: search,
            expand: true,
            limit: offset,
            offset: start
          },

        })
        start += offset;
      } catch (err) {
        console.log({ msg: "error while retreiving apis", start, end: start + offset })
        console.log(err)
        process.exit()
      }
    }

    console.log("waiting for errors")
    await new Promise(r => setTimeout(r, 80000000))
    // return data.list;
  }


  async getApiByResultIndex(index: number, full: boolean = true, search: string = "") {
    return new Promise((res, rej) => {
      const req = this.api.get("/", {
        params: {
          query: search,
          expand: full,
          limit: 1,
          offset: index
        }
      })
      req.then(({ data }) => res(data.list))
        .catch(err => rej(err))
    })
  }

  async searchWithDetails(search: string) {
    let results = [];
    try {
      results = await this.search(search);
    }catch(e){
      console.log(e);
    }

    const detailedResults = [];
    for (let result of results) {
      const details = await this.getDetails(result.id);
      detailedResults.push(details);
    }
    return detailedResults;
  }

  async getDetails(apiId: string) {
    try {
      const { data } = await this.api.get(`/${apiId}`);

      return data;
    }catch(e) {
      return {...e};
    }
  }

  async create(apiInfo: any, shouldPublish?: boolean) {
    try {
      const alreadyExistingApi = await this.exist(apiInfo);

      if (!alreadyExistingApi) {
        if (shouldPublish) {
          // si on doit publier on enlève le lifeCycleStatus d'avant
          var { lifeCycleStatus, provider, ...api } = apiInfo;
        } else {
          var { provider, ...api } = apiInfo;
        }

        // if doesn't exist we create
        var { data: newApiInfo } = await this.api.post("/", {
          ...api,
          // policies: ["Unlimited"], //We need this to publish
        });
        const myApi = newApiInfo || alreadyExistingApi;
        return shouldPublish ? this.publish(myApi) : myApi;
      }else {
        if(shouldPublish && alreadyExistingApi.lifeCycleStatus == 'CREATED') {
          this.publish(alreadyExistingApi)
        } else {
          let myError = { "error": "API exists already " + apiInfo.context + "/" + apiInfo.version + ".", "id": apiInfo.id} ;
          process.stderr.write(JSON.stringify(myError) + "\n");
          return myError
        }
        // return;
      }
    } catch (err: any) {
      // * il n'y a pas de raison mais bon
      // * je ne peux pas mettre l'error handler au niveau d'axios
      // * parce qu'il prends les 404 pour des erreurs
      if (err.response) {
        let myError = err.response.data
        myError.id = apiInfo.id
        // console.log("catching it here " + err.response.data.description)
        process.stderr.write(JSON.stringify(myError) + '\n');
        // console.log(JSON.stringify(myError) + '\n')
        return myError;
      } else {
        let myError = err
        myError.id = apiInfo.id
        // console.log("Or better it here " + err.description)
        process.stderr.write(JSON.stringify(myError) + '\n');
        // console.log(JSON.stringify(myError) + '\n');
        return myError;
      }
    }
  }


  public async createNewVersion(apiInfo: any, shouldPublish?: boolean) {
    try {
      //Getting the already existing version
      const alreadyExistingApi = await this.getAnyVersion(apiInfo);
      // console.log("existing api id " + alreadyExistingApi.id)
      // We try to create this new version
      var { data: newApiInfo } = await this.api.post("/copy-api", null, 
        {
          params:{ 
            apiId: alreadyExistingApi.id,
            newVersion: apiInfo.version,
            defaultVersion: false
            // policies: ["Unlimited"], //We need this to publish
          }
        })
        // console.log("newly copied api id " + newApiInfo.id);
      delete apiInfo.id
      delete apiInfo.owner  
      await this.update(newApiInfo.id, apiInfo);
      try {
        let newlyPublished = shouldPublish ? await this.publish(newApiInfo) : apiInfo;
        return newlyPublished;
      }catch(e) {
        console.log("error publishing new version " + e);
        return apiInfo;
      }
    } catch (err: any) {
      // * il n'y a pas de raison mais bon
      // * je ne peux pas mettre l'error handler au niveau d'axios
      // * parce qu'il prends les 404 pour des erreurs
      if (err.response) {
        let myError = err.response.data
        // console.log("catching it here " + err.response.data.description)
        process.stderr.write(JSON.stringify(myError) + '\n');
        // console.log(JSON.stringify(myError) + '\n')
        return myError;
      } else {
        let myError = err
        // console.log("Or better it here " + err.description)
        process.stderr.write(JSON.stringify(myError) + '\n');
        // console.log(JSON.stringify(myError) + '\n');
        return myError;
      }
    }
  }

  public async getMediatorsFromAPI(apiId: string) {
    try {
      return (await this.api.get(`/${apiId}/mediation-policies`)).data.list;
    }catch(e) {
      console.log(e);
    }
  }


  public async addMediatorByFileName(api: any, type: string, filename: string) {
    try {
      const inlineFile = fs.createReadStream(filename);
      const formData = new FormData();
      formData.append('inlineContent', inlineFile);
      formData.append('type', type);
      const result = await axios({
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        method: 'post',
        url: `${this.conf.serverletURL}/api/am/publisher/v1/apis/${api.id}/mediation-policies`,
        data: formData,
        headers: {
          "Authorization": `Bearer ${this.token}`,
          ...formData.getHeaders()
        }
      })
      if(result.status === 201) {
        let mediationPolicy = {
          mediationPolicies: []
        }
        let plusShared = result.data;
        plusShared.shared = false;
        if(Array.isArray(api.mediationPolicies)){
          api.mediationPolicies.push(plusShared)
        } else {
          mediationPolicy.mediationPolicies.push(plusShared);
          api.mediationPolicies = mediationPolicy.mediationPolicies;
        }
        const republish = await this.update(api.id, api)
        return republish
      }
    }catch(e) {
      console.log(e);
    }
  }


  public async removeAllMediators(api) {
    const { list }: MediatorSearchResult = (await this.api.get(`/${api.id}/mediation-policies`)).data;
    list.map(async (mediator) => {
      try {
        const result = await this.removeMediatorByID(api, mediator.id);
      }catch(e){
        console.log(e)
      }
    })
  }

  public async removeMediatorByID(api, mediatorId) {
    try {
      

      this.removeObjById(api.mediationPolicies, mediatorId);

      if(api.mediationPolicies && api.mediationPolicies.length < 1){
        delete api.mediationPolicies;
      }

      const republish = await this.update(api.id, api)

      const response = await this.api.delete(`/${api.id}/mediation-policies/${mediatorId}`);

      console.log(`Mediator '${mediatorId}' deleted from API with id ${api.id}.`)
      return response;
    }catch(e) {
      console.log(`Error deleting mediator '${mediatorId}' from API with id ${api.id}`, + e);
      return e
    }
  }

  removeObjById(arr, id) {
    try {
      const index = arr.findIndex(obj => obj.id === id);
      if (index > -1) {
        arr.splice(index, 1);
      }
    } catch(e) {
      console.log("Couldn't find mediator with id " + id + " in the API.")
    }
  }


  async delete(apiId: string) {
    const { data } = await this.api.delete(`/${apiId}`);

    return data;
  }

  async deleteByName(name: string) {
    if (name == "*" || name == "build") {
      const apis = await this.search();

      for (let api of apis) {
        try {
          await this.delete(api.id);
        } catch (err: any) {
          console.log(err.response.data);
        }
      }
      return { msg: "done" };
    }

    const apis: any[] = await this.search(`name:${name}`);

    const selected = apis.find((api) => api.name == name);

    if (selected) {
      await this.delete(selected.id);
      return { msg: "done" };
    }

    return { msg: "not matching api found" };
  }

  /**
   * Publish the API
   * @param apiInfo 
   * @returns the expanded API NDJSON
   */
  async publish(apiInfo: any) {
    try {
      // * si l'api n'a pas de policy on la rajoute
      if (!apiInfo.policies) {
        await this.update(apiInfo.id, { policies: ["Unlimited"] });
      }

      // * on passe le lifecycle à publish
      console.log("publishing new api version")
      const { data } = await this.api.post(`/change-lifecycle`, null, {
        params: {
          apiId: apiInfo.id,
          action: "Publish", 
        },
      });
    } catch (err: any) {
      console.log(err.response.data);
      // process.exit();
    }

    return await this.getDetails(apiInfo.id);
  }

  /**
   * Will deprecate the API in stdin
   * @param apiInfo NDJSON API
   * @returns the new API as NDJSON
   */
  async deprecate(apiInfo: any) {


    const apiId = apiInfo.id || apiInfo
    // console.log("deprecating " + apiId);

    const paramsDeprecate = new URLSearchParams({
      apiId,
      action: "Deprecate"
    }).toString();

    // on essais de changer le lifecycle à deprecate
    let url = "/change-lifecycle?" + paramsDeprecate

    try {
      await this.api.post(url, {}, {});
    } catch {
      process.stderr.write(JSON.stringify({ error: "while changing lifecycle to Deprecate" }))
    }

    const paramsRetire = new URLSearchParams({
      apiId,
      action: "Retire"
    }).toString();

    url = "/change-lifecycle?" + paramsRetire

    // puis on essais de le passer à retire
    try {
      await this.api.post(url, {}, {});
    } catch (err: any) {
      process.stderr.write(JSON.stringify({ error: "while changing lifecycle to Retire" }))
    }

    return await this.getDetails(apiId);
  }
  /**
   * tell us if the api already exist
   * @param apiInfo an object that give the name context and version of the api
   * @returns false or the found API as NDJSON
   */
  async exist(apiInfo: any): Promise<any> {
    const result = await this.search(
      `context:${apiInfo.context}/${apiInfo.version}`
      // `name:${apiInfo.name}` // context:${apiInfo.context} version:${apiInfo.context} // bizarre ça marche pas avec ça en plus
    );
    // console.log("oui1");
    return result[0] || false;
  }


  /**
   * Tries to get an existing version
   * @param apiInfo an object that give the name context and version of the api
   * @returns false or the found API as NDJSON
   */
  async getAnyVersion(apiInfo: any): Promise<any> {
    // console.log("Getting previous version of " + apiInfo.context + "/" + apiInfo.version)
    let result;
    try {
      result = await this.search(
        `context:${apiInfo.context}`
        // `name:${apiInfo.name}` // context:${apiInfo.context} version:${apiInfo.context} // bizarre ça marche pas avec ça en plus
      );
    }catch(error) {
      console.log("error searching " + error);
    }
    // console.log("oui1");
    // console.log("One such previous version id " + result[0].id)
    return result[0] || false;
  }

  async update(apiId: string, update: any) {
    let error = "";
    let updatedAPI = undefined;


    let retries = 5;
    let success = false;
    while (retries > 0 && !success) {
      try {
        const response = await this.api.put(`/${apiId}`, update)
        if (response.status >= 200 && response.status < 400) {
          success = true;
          updatedAPI = response;
          break;
        }
      } catch (e) {
        console.log(e);
        error = e
      }
      retries--;
    }

    return updatedAPI.data || error;
  }
}

class Mediator {
  public name?: string;
  public id?: string;
  public type: string;
}

class MediatorSearchResult {
  public count?: number;
  public list?: Array<Mediator>;
  public pagination?: {
    offset: number,
    limit: number;
    total: number;
    next: string;
    previous: string;
  }
}

export default PublisherAPIClient;
