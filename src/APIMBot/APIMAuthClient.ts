import axios from "axios";
import https from "https";

import { AppConfig } from "../types/Config";
import { getConfig } from "../Utils";

const config = getConfig();

const SCOPES = [
  "apim:api_view",
  "apim:api_create",
  "apim:api_publish",
  "apim:api_import_export",
  "apim:subscribe",
  "apim:app_manage",
  "apim:api_delete",
  "apim:subscribe",
  "apim:app_manage",
  "apim:api_key",
  "apim:sub_manage",
].join(" ");

class APIMAuthClient {
  protected conf: AppConfig;
  private gatewayAPI: any;
  private basicToken: string;

  constructor(configName: string) {
    // load api credential from the config file
    const conf = config.environments.find((cfg) => cfg.name == configName);

    // throw error if no matchin api conf name
    if (!conf) {
      throw new Error("no conf for " + configName);
    }

    // extract all we need
    const { clientId, clientSecret, gatewayURL } = conf;
    this.conf = conf; // we save it for later

    this.gatewayAPI = axios.create({
      baseURL: gatewayURL,
      httpsAgent: new https.Agent({ rejectUnauthorized: false }),
    });

    // premade the basic token
    // console.log(`${clientId}:${clientSecret}`);
    const buffer = Buffer.from(`${clientId}:${clientSecret}`);
    this.basicToken = buffer.toString("base64");
  }

  /**
   * @returns the BearerToken form the config file credentials
   */
  async getToken(): Promise<string> {
    try {
      const { data } = await this.gatewayAPI.post("/token", null, {
        params: {
          grant_type: "client_credentials",
          scope: SCOPES,
        },
        headers: {
          Authorization: `Basic ${this.basicToken}`,
        },
      });

      return data.access_token;
    } catch (err: any) {
      if (err.response) {
        console.log(err.response.data);
      } else {
        console.log(err.toString());
      }

      console.error("error while fetching user token");
      process.exit();
    }
  }
}

export default APIMAuthClient;
